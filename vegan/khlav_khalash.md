# Khlav Khalash

Wer kennt das nicht: Du bist in einer fremden Stadt unterwegs und plötzlich
überkommt dich der Heißhunger nach frischem Khlav Khalash.
Im Folgenden zeige ich euch mein veganes Khlav Khalash Rezept nach Familientradition.

## Zutaten
* 1 rote Zwiebel
* Sojamedallions (4 mal Anzahl der Schichten eurer Zwiebel)
* 2 rote Spitzpaprika
* 4 Cherrytomaten
* 4 Metallspieße
* helle Sojasauce & dunkle Sojasauce
* 1 TL Sambal Oelek
* Olivenöl
* Kräuter (Oregano, Rosmarin, Thymian)
* Kreuzkümmel (Pulver, vorher kurz anrösten)
* Knoblauch


## Zubereitung
1. Sojamedallions der Packungsanweisung nach in Gemüsebrühe einweichen und
ausdrücken.
2. Sojamedallions einlegen in einer Brühe aus heller & dunkler Sojasauce (1:1),
gehacktem Knoblauch, Kreuzkümmel und Sambal Oelek.
3. Zwiebel vierteln und Paprika in etwa 2cm x 2cm Quadrate schneiden
4. Ofen (Grillfunktion) vorheizen auf 200°C
5. Während der Ofen vorheizt: Spieße machen. Reihenfolge:
Zwiebel, Paprika, Sojafleisch. Ihr nehmt zuerst eines der größeren Medallions
und verringert dann in jedem Durchgang die Größe (ebenso bei Zwiebel und Paprika)
damit sich der Spieß nach vorne hin verjüngt. An die Spitze steckt ihr eine Cherrytomate.
Das sollte insgesamt vier Spieße ergeben.
6.Spieße in eine Feuerfeste Form legen, am besten so, dass sie den Boden nicht
berühren. Anschließend bestreut ihr die Spieße mit Kräutern und Olivenöl.
7. Etwa 1/2 Stunde in den Ofen. Geht auch gut auf dem Grill.
