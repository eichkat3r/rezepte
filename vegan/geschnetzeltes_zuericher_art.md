# Soja-Geschnetzeltes Züricher Art

Das Tolle an Sojaschnetzeln ist, dass sie schön weich werden und nicht so
eklig trocken und sehnig sind wie die Fleischschnetzel, die es in der Mensa gibt.
Daher hat dieses Gericht gegenüber der Fleischvariante einen klaren geschmacklichen
Vorteil, und ihr könnt wenig falsch machen.


Das Rezept reicht für eine Person.
Dazu passen Spätzle, Rösti oder Kartoffelknödel.
Ich mag keine Pilze – bei Bedarf einfach zwischen Schritt 3 und 4 ergänzen.

## Zutaten

Mengenangaben nach Gefühl.

* Sojaschnetzel – 1 Hand voll
* Zwiebel, gehackt – eine
* Petersilie, gehackt
* Paprika edelsüß – etwa 1 TL
* Kreuzkümmel – etwa 1 TL
* kräftige Gemüsebrühe – etwa 1,5L
* Weißwein, etwa 1 Glas
* Sahneersatz (Sojasahne, Hafersahne o.ä.) – ca. 200ml
* Weizenmehl – 1 EL

## Zubereitung

1. Sojaschnetzel in der Brühe ziehen lassen, bis sie weich sind (ggf. nach Packungsanweisung mehrere Minuten in der Brühe einweichen).
2. Brühe abgießen (aufbewahren!) und Sojaschnetzel ausdrücken. Ausgedrückte Sojaschnetzel gut mit Paprikapulver und Kreuzkümmel mischen.
3. gehackte Zwiebel in Pflanzenöl (geschmacksneutral) in einer tiefen Pfanne anbraten. Sobald die Zwiebeln glasig sind, Mehl dazugeben und kurz andünsten.
4. Sojaschnetzel zu den Zwiebelstücken in die Pfanne geben. Nicht zu viel umrühren, damit sie gut Farbe nehmen können.
5. Wenn die Sojaschnetzel eine leichte Kruste bekommen, alles mit Weißwein ablöschen. Das gibt einen schönen Geschmack. Ihr könnt den Alkohol ruhig etwas rauskochen lassen.
Leider kenne ich keine gute Alternative ohne Alkohol – vielleicht klappt es auch mit hellem Traubensaft.
6. Sahneersatz und Petersilie hinzufügen, Hitze reduzieren.
7. Mit Salz, Pfeffer und Muskat abschmecken.
