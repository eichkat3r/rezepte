# Vegane Käsecreme

Veganer Käse aus dem Regal schmeckt oft komisch.
Deshalb habe ich versucht, meine eigene Käsecreme herzustellen.

Disclaimer: Aktuell bin ich noch nicht zu 100% zufrieden damit.
Auch wenn die Creme gut schmeckt, gibt es noch Luft nach oben.
Der Geschmack wird vor allem durch die Hefeflocken bestimmt und durch Zugabe von
Essig abgerundet. Sojasauce bringt noch ein bisschen zusätzliches Umami.

Passt sehr gut zu Nudeln mit Tomatensauce, oder als Dip für Pommes, Chips usw.
Insgesamt ist die Konsistenz ähnlich zu Odenwälder Kochkäs', d.h. die Creme
kann ähnlich verwendet werden (z.B. als Brotaufstrich oder mit veganem Schnitzel).

## Zutaten

Mengen sind grob geschätzt und können mitunter stark abweichen.

* Margarine – 2 EL
* Mehl – 1 EL
* Hafermilch – ca. 150 ml
* Hefeflocken – 1 EL
* Helle Sojasauce – 1 TL
* Malzessig – 1 EL
* Salz & Pfeffer
* Optional: Basilikum, Oregano


## Zubereitung

1. Zuerst machst du eine Art vegane Bechamel. Dazu erhitzt du die Margarine, bis sie flüssig wird.
2. Zur flüssigen Butter gibst du das Mehl und verrührst Mehl und Butter mit einem Schneebesen zu einer homogenen Paste.
3. Topf vom Herd nehmen und Hafermilch einrühren.
4. Danach gibst du die restlichen Zutaten dazu und verrührst das ganze gut. Wenn die Masse zu dickflüssig ist,
kannst du noch mehr Hafermilch dazugeben.

