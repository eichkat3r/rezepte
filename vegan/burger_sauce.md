# Hamburger-Sauce

Eigentlich ist dieses Grundrezept viel zu trivial.
Allerdings finde ich, dass das Angebot an veganen Saucen viel zu klein ist
und es deshalb wichtig ist zu wissen, wie man eine einfache Grundsauce für
Fast Food wie Burger oder Pommes selbst macht.

## Zutaten Grundsauce

* Vegane Majo aka "Salatcreme" – 2 EL
* Gurkenwasser – 1 Schluck
* Senf, mittelscharf – 1/2 TL

## Zubereitung

Die Zutaten für die Grundsauce werden verrührt.

Danach kannst du dich entscheiden, in welche Richtung die Sauce gehen soll.
Folgende Zutaten finde ich interessant:

* "Hummus-Gewürz" von Just Spices. Passt gut zu Patties auf Falafel-Basis.
* Currypulver. Gibt der Sauce eine schöne, kräftige Farbe. Das nehme ich gerne,
wenn ich Burger auf Basis eines Seitan- oder Soja-patties mit Grillgemüse mache.
* Tomatenpüree. Dadurch wird die Sauce schön orange-rot, wie eine Cocktailsauce.
Dazu passt auch Dill. Für mehr Würze mische ich auch gerne noch Sambal Oelek oder
scharfen Ayvar dazu. Passt dann sehr gut zu Pommes.
* 1/2 TL Trüffelöl. Passt gut zu Süßkartoffelpommes.

